package com.chess.pieces;

import com.chess.Alliance;

import java.util.List;

public abstract class Piece {
    private Alliance colour;
    private int positionX;
    private int positionY;
    private List<Move> LegalMoves;

    public abstract List<Move> calculateLegalMove();

    public Alliance getColour(){
        return this.colour;
    }
}
