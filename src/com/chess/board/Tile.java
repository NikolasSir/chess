package com.chess.board;

import com.chess.Pair;
import com.chess.pieces.Piece;

import java.util.List;

public class Tile {
    private final Pair<Integer, Integer> coordinate;
    private Piece piece;
    private final List<Pair> chessCoordinates;

    protected Tile(final Pair coordinate, Piece piece){
        this.coordinate = coordinate;
        this.piece = piece;
    }

    public boolean isOccupied(Tile tile){
        return tile.piece == null ? false : true;
    }

    public Piece getPiece(Tile tile){
        return this.piece;
    }

    public void setPiece(Piece piece){
        this.piece = piece;
    }

    public Pair<Integer, Integer> getCoordinate() {
        return coordinate;
    }

    public static void removePiece(Tile tile){
        tile.piece = null;
    }
}
