package com.chess.board;

import com.google.common.collect.ImmutableList;

import java.util.List;

public final class Board {
    private static ImmutableList<Tile> tiles;
    private static final Board BOARD = new Board(tiles);

    private Board(List<Tile> tiles){
        this.tiles = createEmptyBoard(tiles);
    }

    public static Board getBoard(){
        return BOARD;
    }

    private static ImmutableList<Tile> createEmptyBoard(List<Tile> tiles){
        for (int i = 1; i <= 8; i++){
            for (int j = 1; j <= 8; j++){
                tiles.add(new Tile(i, j, null));
            }
        }
        return ImmutableList.copyOf(tiles);
    }
}
