package com.chess;

public class Pair<U, V>{
    private final U x;
    private final V y;

    Pair(final U x, final V y){
        this.x = x;
        this.y = y;
    }

    public U getX() {
        return this.x;
    }

    public V getY() {
        return this.y;
    }
}
